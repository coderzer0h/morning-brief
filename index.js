global.__basedir = __dirname + "/";
global.__libdir = __basedir + "lib/";
global.utilities = require(__libdir + "utilities");

var arguments = require('commander');

arguments
  .version('0.0.1')
  .option('-m, --module [value]', 'module to run')
  .parse(process.argv);

  if (arguments.module) {
      var module = require( __libdir + arguments.module);
      module.execute();
  }
