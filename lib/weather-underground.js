var config = require(__basedir + 'config.json');

module.exports = {
  execute : function() {
    api = config.weatherUnderground;
    url = api.endpoint + api.apiKey + '/forecast/q/' + api.state + "/" + api.city + ".json";
    utilities.simpleGet(url)
    .then((html) => console.log(html))
    .catch((err) => console.error(err));
  }
}
